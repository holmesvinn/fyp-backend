const mongoose  = require('mongoose');
const Schema = mongoose.Schema;


  

const DeviceSchema = new Schema({
    device_name: String,
    device_cost: String,
    location: String,
    from_org: String,
    tracking_hardware: String,
    device_spec: String,
    device_usage: String
});

const Devices = mongoose.model('devices',DeviceSchema);
module.exports = Devices;