const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors')



const app = express(); 

app.use(cors());

mongoose.connect('mongodb://localhost/tags',{useNewUrlParser:true});

app.use(bodyParser.json());

app.use('/api/track',require('./routes/devices'));

app.listen(3000);
console.log("backend server started");