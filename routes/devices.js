const express = require('express');
const Devices = require('../models/devices');
const routes = express.Router();

routes.get('/all',function(req,res){
    Devices.find().then(function(result){
        res.send(result);
    })
});

routes.get('/:deviceId',function(req,res){
    Devices.findOne({_id:req.params.deviceId}).then(function(result){
                res.send(result);  
                
    }).catch(function(err){
        console.log("reading error");
        res.send('error');

    });
})

routes.post('/device', function(req,res){
    console.log(req.body);
    Devices.create(req.body).then(function(devices){
        res.send(devices);
    }).catch(function(err){
        res.send(err.message);
    });
});

routes.put('/device/:deviceId', function(req,res){
    Devices.findOneAndUpdate({_id:req.params.deviceId},req.body).then(function(){
        Devices.findOne({_id:req.params.deviceId}).then(function(devices){
            res.send(devices)
        }).catch(function(err){
            res.send(err.message);
    });
    })
})



module.exports = routes;